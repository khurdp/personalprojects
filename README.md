### Personal Projects

I will list my projects here:
[//]: # (This may be the most platform independent comment)

[//]: # (1. https://bitbucket.org/khurdp/webapplication2.........on: https://webapplicaion2.azurewebsites.net/)
[//]: # (1. https://bitbucket.org/khurdp/edureka.................on: https://protected-spire-22056.herokuapp.com/)
[//]: # (1. Ruby on rails (Will list the git soon)...............on: https://fierce-chamber-9200.herokuapp.com/)
[//]: # (1. WebEDI - MEAN fullstack. (Cloud9 workspace) ...........................on: https://c9.io/khurdp/web_edi/)
[//]: # (1. Simple flight search - CEAN fullstack ...................... coming soon)
[//]: # (1. News Application using NY times API .......................... coming soon)

| Brief description (details in bitbucket or c9 readme) | bitbucket (git)                                  |  cloud 9 workspace            |  Hosted   |
| ----------------------------------------------------- |:------------------------------------------------:|:-----------------------------:| ---------:|
| OAuth token based authentication. Added refresh token.| https://bitbucket.org/khurdp/owin-token-authentication |
| Fullstack NgRx News Application using NY times API    | https://bitbucket.org/khurdp/ngrxnewsapplication |                               | https://guarded-mountain-68545.herokuapp.com/
| Simple flight search - CEAN fullstack                 |            coming soon                           |      coming soon              | 
| WebEDI - MEAN fullstack. (Work in progress)           |                                                  | https://c9.io/khurdp/web_edi/ | 
| Frontend HTML5/CSS3 Course                            | https://bitbucket.org/khurdp/edureka             |                               | https://protected-spire-22056.herokuapp.com/
| Asp.net core (Asp.net 5), MVC 6, C#, Angular 4 and EF | https://bitbucket.org/khurdp/webapplication2     |                               | https://webapplicaion2.azurewebsites.net/
| Ruby on rails                                         |                                                  |                               | https://fierce-chamber-9200.herokuapp.com/


thank you for visiting :-)

---

Cheatsheet below till i master mark up in this mark down file :-)

https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet